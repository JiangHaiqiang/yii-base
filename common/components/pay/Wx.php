<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午9:06
 */

namespace common\components\pay;

use sdk\pay\wx\JsApiPay;
use sdk\pay\wx\WxPayApi;
use sdk\pay\wx\WxPayUnifiedOrder;
use sdk\pay\wx\WxPayConfig;

/**
 * Class Wx
 * @package common\components\pay
 * User Jiang Haiqiang
 */
class Wx extends Pay
{
    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $key;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $appSecret;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $sslCertPath;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $sslKeyPath;

    /**
     * @var WxPayConfig
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected $_config;

    /**签名
     * @param string $orderId
     * @return mixed|string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function sign($orderId)
    {
        // TODO: Implement sign() method.
        return hash('sha256',md5($this->platId.$this->signKey.$orderId));
    }



    /**
     * @param $openId
     * @param $orderId
     * @param $productName
     * @param $totalMoney
     * @return array
     * @throws \sdk\pay\wx\WxPayException
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function wxPublic($openId,$orderId,$productName,$totalMoney)
    {
        $input = $this->_commonInput($orderId,$productName,$totalMoney);
        $input->SetNotify_url($this->notifyUrl);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $this->_config = new WxPayConfig();

        $prepayOrder = WxPayApi::unifiedOrder($this->_config, $input);

        \Yii::info(json_encode($prepayOrder,JSON_UNESCAPED_UNICODE));

        $response = [
            'status' => '500',
            'data'   => [
                'params' => []
            ],
            'msg'    => '预支付失败'
        ];

        $dealStatus = $this->_commonDealPrepayOrder($prepayOrder);
        if($dealStatus) {
            $params = (new JsApiPay())->GetJsApiParameters($prepayOrder);
            $response = [
                'status' => '200',
                'data'   => [
                    'params' => $params
                ],
                'msg'    => '预支付成功'
            ];
        }

        return $response;
    }

    /**
     * @param $orderId
     * @param $productName
     * @param $totalMoney
     * @return WxPayUnifiedOrder
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected function _commonInput($orderId,$productName,$totalMoney)
    {
        $input = new WxPayUnifiedOrder();
        $input->SetBody($this->bodyPrefix.$productName);
        $input->SetAttach(json_encode(['sign' => $this->sign($orderId),'id'=> $orderId ],JSON_UNESCAPED_UNICODE));
        $input->SetOut_trade_no($orderId);
        $input->SetTotal_fee(bcmul($totalMoney,100));
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 3600));
        return $input;
    }

    /**
     * @param array $prepayOrder
     * @return bool
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected function _commonDealPrepayOrder($prepayOrder)
    {
        if(!isset($prepayOrder['prepay_id'], $prepayOrder['return_code'], $prepayOrder['sign'], $prepayOrder['mch_id'])) {
            return false;
        }

        if($prepayOrder['return_code'] !== 'SUCCESS' || $prepayOrder['mch_id'] !== $this->_config->GetMerchantId()) {
            return false;
        }

        //保存预支付信息
        //预支付id
        //$prepayOrder['prepay_id'];
        //订单号签名
        //$prepayOrder['sign'];
        return true;
    }
}