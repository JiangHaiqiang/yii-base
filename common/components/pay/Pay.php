<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午8:58
 */

namespace common\components\pay;

/**
 * Class Pay
 * @package common\components\pay
 * User Jiang Haiqiang
 */
abstract class Pay
{
    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $platId;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $appId;

    /**
     * @var string 商户id
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $merchantId;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $bodyPrefix;

    /**
     * @var
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $signKey;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $notifyUrl;

    /**
     * @param string $orderId
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    abstract public function sign($orderId);
}