<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午4:12
 */

namespace common\components\other_login;

use common\helpers\ComHelper;
use common\models\User;

/**第三方登陆
 * Class Login
 * @package common\components\other_login
 * User Jiang Haiqiang
 */
abstract class Login
{
    /**
     * @var string
     */
    public $appId;

    /**
     * @var string
     */
    public $appSecret;

    /**
     * @var string
     */
    public $authCallbackUrl = '';

    /**
     * @var User
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected $_user;

    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected $_openid_field;

    /**
     * @var array
     */
    public static $FAIL =[
        'status'    => '500',
        'data'      => [],
        'message'   => 'FAIL'
    ];

    /**
     * @var array
     */
    public static $SUCCESS =[
        'status'    => '200',
        'data'      => [],
        'message'   => 'SUCCESS'
    ];

    /**
     * @return mixed
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    abstract public function auth();

    /**
     * @return mixed
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    abstract public function authCallback();

    /**
     * @param array $params
     * @return mixed
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    abstract public function getAccessToken($params=[]);

    /**根据openid从数据库里获取用户信息
     * @param string $openid
     * @return User|null
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function getUserByOpenIdFromDb($openid)
    {
        $this->_user = User::findOne([
            $this->_openid_field => $openid
        ]);

        return $this->_user;
    }

    /**格式化用户信息
     * @param string  $nickname
     * @param string  $headImg
     * @param string  $openid
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function fillUserInfo($nickname,$headImg,$openid)
    {
        if(empty($this->_user)) {
            $this->_user = new User();
        }

        $this->_user->name     = $nickname;
        $this->_user->head_img = $headImg;
        $this->_user->setAttribute($this->_openid_field,$openid);
        $this->_user->last_login_time = date('Y-m-d H:i:s');
        $this->_user->last_login_ip   = ComHelper::getClientIp();
    }
}