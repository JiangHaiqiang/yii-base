<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午4:13
 */

namespace common\components\other_login;

use common\components\curl\Client;
use common\helpers\ComHelper;

/**微信公众号登陆
 * Class WxPublic
 * @package common\components\other_login
 * User Jiang Haiqiang
 */
class WxPublic extends Login
{
    /**
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected $_openid_field = 'wx_public_open_id';

    /**
     * @var array
     */
    public static $STATUS_MAP=[
        '10003' =>	'redirect_uri域名与后台配置不一致',
        '10004' =>	'此公众号被封禁',
        '10005' =>	'此公众号并没有这些scope的权限',
        '10006' =>	'必须关注此测试号',
        '10009' =>	'操作太频繁了，请稍后重试',
        '10010' =>	'scope不能为空',
        '10011' =>	'redirect_uri不能为空',
        '10012' =>	'appid不能为空',
        '10013' =>	'state不能为空',
        '10015' =>	'公众号未授权第三方平台，请检查授权状态',
        '10016' =>	'不支持微信开放平台的Appid，请使用公众号Appid',
    ];

    /**微信登录跳转
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    public function auth()
    {
        $authUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?';
        $params = [
            'appid'         => $this->appId,
            'redirect_uri'  => $this->authCallbackUrl,
            'response_type' => 'code',
            'scope'         => 'snsapi_userinfo',
            'state'         => '123#wechat_redirect'
        ];
        $authUrl.= http_build_query($params);
        header('Location:' . $authUrl);
        exit();
    }

    /**微信登录回调
     * @return \common\models\User|null
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function authCallback()
    {
        $code  = ComHelper::fStr('code',$_GET);
        $state = ComHelper::fStr('state',$_GET);

        if(isset(self::$STATUS_MAP[ $state ])) {
            if(YII_DEBUG) {
                exit(self::$STATUS_MAP[ $state ]);
            }
            exit('非法回调');
        }

        if(!empty($code)) {
            $accessTokenInfo = $this->getAccessToken(['code'=>$code]);
            if($accessTokenInfo['status'] === '200') {

                $this->getUserByOpenIdFromDb($accessTokenInfo['data']['openId']);

                if(empty($this->_user)) {
                    $userInfo = $this->catchUserInfoByAccessTokenAndOpenId(
                        $accessTokenInfo['data']['accessToken'],
                        $accessTokenInfo['data']['openId']
                    );

                    if($userInfo['status'] == '200') {
                        $this->fillUserInfo(
                            $userInfo['data']['nickname'],
                            $userInfo['data']['headimgurl'],
                            $userInfo['data']['openid']
                        );

                        /**
                         * 保存用户信息
                         */
                        $this->_user->save();
                    }
                }

                return $this->_user;
            }
        }
    }

    /**
     * @param array $params
     * @return array
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    public function getAccessToken($params=[])
    {
        $accessTokenUrl = 'https://api.weixin.qq.com/sns/oauth2/access_token?';

        $params = [
            'appid'         => $this->appId,
            'secret'        => $this->appSecret,
            'code'          => $params['code'],
            'grant_type'    => 'authorization_code'
        ];

        $accessTokenUrl .= http_build_query($params);

        $response = (new Client($accessTokenUrl))->get();

        $return = static::$FAIL;
        if($response->success()) {
            $data = $response->jsonBody2Array();
            $return['data'] = $data;
            if(isset($data['access_token'],$data['openid'])) {
                $return = self::$SUCCESS;
                $return['data'] = [
                    'accessToken' => $data['access_token'],
                    'openId'      => $data['openid']
                ];
            }
        }

        return $return;
    }

    /**
     * @param $accessToken
     * @param $openId
     * @return array
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    public function catchUserInfoByAccessTokenAndOpenId($accessToken,$openId)
    {
        $catchUrl = 'https://api.weixin.qq.com/sns/userinfo?';
        $params = [
            'access_token' => $accessToken,
            'openid'       => $openId,
            'lang'         => 'zh_CN'
        ];
        $catchUrl .= http_build_query($params);

        $response = (new Client($catchUrl))->get();

        $return = static::$FAIL;
        if($response->success()) {
            $data = $response->jsonBody2Array();
            $return['data'] = $data;
            if(isset($data['nickname'])) {
                $return = static::$SUCCESS;
                $return['data'] = $data;
            }
        }

        return $return;
    }

    /**
     * @return string
     * @author   Jiang Haiqiang
     * @email    jhq0113@163.com
     */
    public function getClientCredentialAccessToken()
    {
        $accessTokenUrl ='https://api.weixin.qq.com/cgi-bin/token?';
        $params = [
            'grant_type' => 'client_credential',
            'appid'      => $this->appId,
            'secret'     => $this->appSecret
        ];
        $accessTokenUrl .= http_build_query($params);
        $response = (new Client($accessTokenUrl))->get();

        if($response->success()) {
            $data =$response->jsonBody2Array();
            if(isset($data['access_token'])) {
                return $data['access_token'];
            }
        }
        return '';
    }
}