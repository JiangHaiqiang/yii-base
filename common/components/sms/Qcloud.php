<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午3:39
 */

namespace common\components\sms;

use sdk\sms\qcloud\SmsMultiSender;
use sdk\sms\qcloud\SmsSingleSender;

/**
 * Class Qcloud
 *
 * $sms = \Yii::$app->sms->sendTemplate('13718462968','ORDER',['username'=>'海强']);
 *
 * @package common\components
 * User Jiang Haiqiang
 */
class Qcloud extends Sms
{
    /**
     * @param $result
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function formatResult($result)
    {
        //$result = json_decode($result,true);
        return $result;
    }

    /**
     * @param $phone
     * @param $msg
     * @param string $type
     * @param string $nativeCode
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function sendOne($phone,$msg,$type=0)
    {
        $client = new SmsSingleSender($this->appId,$this->appKey);
        $result = $client->send($type,$this->nativeCode,$phone,$msg);
        return $this->formatResult($result);
    }

    /**
     * @param $phones
     * @param $msg
     * @param int $type
     * @param string $nativeCode
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function sendSome($phones,$msg,$type=0,$nativeCode='86')
    {
        $client = new SmsMultiSender($this->appId,$this->appKey);
        $result = $client->send($type,$this->nativeCode,$phones,$msg);
        return $this->formatResult($result);
    }

    /**
     * @param $phone
     * @param $templateId
     * @param $data
     * @param string $type
     * @param string $nativeCode
     * @return bool|mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function sendTemplate($phone,$templateId,$data,$type=0)
    {
        $msg = $this->getMsg($templateId,$data);

        if(!empty($msg)) {
            if(is_array($phone)) {
                return $this->sendSome($phone,$msg,$type);
            }
            return $this->sendOne($phone,$msg,$type);
        }

        return false;
    }
}