<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午3:39
 */

namespace common\components\sms;

use common\helpers\StringHelper;

/**
 * Class Sms
 * @package common\components
 * User Jiang Haiqiang
 */
abstract class Sms
{
    /**
     * @var
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $appId;

    /**
     * @var
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $appKey;

    /**国家码
     * @var string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $nativeCode = '86';

    /**
     * @var
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $sign;

    /**
     * @var array
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $template=[];

    /**从自定义模板中获取信息
     * @param string $templateId
     * @param array  $data
     * @return string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function getMsg($templateId,$data=[])
    {
        if(!isset($this->template[ $templateId ])) {
            return '';
        }
        $msg = StringHelper::interpolate($this->template[ $templateId ],$data);
        return $msg.$this->sign;
    }

    /**格式化发送结果
     * @param mixed $result
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    abstract public function formatResult($result);

    /**给一个人发送短信
     * @param string $phone
     * @param string $msg
     * @param int    $type
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    abstract public function sendOne($phone,$msg,$type=0);

    /**给多人发送短信
     * @param array  $phones
     * @param string $msg
     * @param int    $type
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    abstract public function sendSome($phones,$msg,$type=0);

     /**发送模板短信
     * @param string $phone
     * @param string $templateId
     * @param mixed  $data
     * @param string $type
     * @return bool|mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
     abstract public function sendTemplate($phone,$templateId,$data,$type=0);
}