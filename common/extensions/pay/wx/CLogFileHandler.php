<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/10/14
 * Time: 下午7:21
 */

namespace sdk\pay\wx;


class CLogFileHandler implements ILogHandler
{
    private $handle = null;

    /**
     * CLogFileHandler constructor.
     * @param string $file
     */
    public function __construct($file = '')
    {
        $this->handle = @fopen($file,'a');
    }

    /**
     * @param $msg
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function write($msg)
    {
        @fwrite($this->handle, $msg, 4096);
    }

    /**
     *
     */
    public function __destruct()
    {
        @fclose($this->handle);
    }
}