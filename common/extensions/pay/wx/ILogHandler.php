<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/10/14
 * Time: 下午7:21
 */

namespace sdk\pay\wx;

//以下为日志

interface ILogHandler
{
    public function write($msg);

}