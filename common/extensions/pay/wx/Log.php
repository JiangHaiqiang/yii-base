<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/10/14
 * Time: 下午7:23
 */

namespace sdk\pay\wx;

/**
 * Class Log
 * @package common\vendor\pay\weixin
 * User Jiang Haiqiang
 */
class Log
{
    /**
     * @var ILogHandler
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    private $handler = null;
    private $level = 15;

    /**
     * @var self
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    private static $instance = null;

    private function __construct(){}

    private function __clone(){}

    /**
     * @param null $handler
     * @param int $level
     * @return Log
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function Init($handler = null,$level = 15)
    {
        if(!self::$instance instanceof self)
        {
            self::$instance = new self();
            self::$instance->__setHandle($handler);
            self::$instance->__setLevel($level);
        }
        return self::$instance;
    }


    private function __setHandle($handler){
        $this->handler = $handler;
    }

    private function __setLevel($level)
    {
        $this->level = $level;
    }

    public static function DEBUG($msg)
    {
        self::$instance->write(1, $msg);
    }

    public static function WARN($msg)
    {
        self::$instance->write(4, $msg);
    }

    /**
     * @param $msg
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function ERROR($msg)
    {
        $debugInfo = debug_backtrace();
        $stack = "[";
        foreach($debugInfo as $key => $val){
            if(array_key_exists("file", $val)){
                $stack .= ",file:" . $val["file"];
            }
            if(array_key_exists("line", $val)){
                $stack .= ",line:" . $val["line"];
            }
            if(array_key_exists("function", $val)){
                $stack .= ",function:" . $val["function"];
            }
        }
        $stack .= "]";
        self::$instance->write(8, $stack . $msg);
    }

    /**
     * @param $msg
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function INFO($msg)
    {
        self::$instance->write(2, $msg);
    }

    /**
     * @param $level
     * @return string
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    private function getLevelStr($level)
    {
        switch ($level)
        {
            case 1:
                return 'debug';
                break;
            case 2:
                return 'info';
                break;
            case 4:
                return 'warning';
                break;
            case 8:
                return 'error';
                break;
            default:
                return 'info';
        }
    }

    /**
     * @param $level
     * @param $msg
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    protected function write($level,$msg)
    {
        $level = $this->getLevelStr($level);

        //用Yii打日志
        call_user_func('\Yii::'.$level,$msg);
    }
}