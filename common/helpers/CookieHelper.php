<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午8:38
 */

namespace common\helpers;

use yii\web\Cookie;

/**
 * Class CookieHelper
 * @package common\helpers
 * User Jiang Haiqiang
 */
class CookieHelper
{
    /**
     * @param string $name
     * @param bool   $defaultValue
     * @return mixed
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function get($name,$defaultValue=false)
    {
        return \Yii::$app->request->cookies->getValue($name,$defaultValue);
    }

    /**
     * @param string $name
     * @param string $value
     * @param int    $timeout
     * @param string $domain
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function set($name,$value,$timeout=DateHelper::ONE_WEEK,$domain=null)
    {
        $cookie = new Cookie();

        $cookie->value      = $value;
        $cookie->name       = $name;
        $cookie->expire     = time() + $timeout;
        $cookie->httpOnly   = true;

        if(isset($domain)) {
            $cookie->domain = $domain;
        }
        return \Yii::$app->response->cookies->add($cookie);
    }

    /**
     * @param string $name
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function del($name)
    {
        return \Yii::$app->response->cookies->remove($name);
    }

    /**
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public static function delAll()
    {
        return \Yii::$app->response->cookies->removeAll();
    }

}