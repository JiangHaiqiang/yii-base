<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午8:40
 */

namespace common\helpers;

/**
 * Class DateHelper
 * @package common\helpers
 * User Jiang Haiqiang
 */
class DateHelper
{
    private function __construct()
    {
    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    /**
     * 一分钟
     */
    const ONE_MINUTE = 60;

    /**
     * 十分钟
     */
    const TEN_MINUTE = 600;

    /**
     * 半小时
     */
    const HALF_HOUR  = 1200;

    /**
     * 一小时
     */
    const ONE_HOUR   = 3600;

    /**
     * 一天
     */
    const ONE_DAY = 86400;

    /**
     * 一周
     */
    const ONE_WEEK = 604800;
}