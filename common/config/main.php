<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language'   => 'zh-CN',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        //短信服务
        'sms'   => [
            'class' => 'common\components\sms\Qcloud',
            'sign'      => '【83cloud】',
            'template'  => require __DIR__.'/sms-template.php',
            'appId'     => '14001563234',
            'appKey'    => 'a11c4b2262asdfasf',
        ],
        /**
         * 微信支付
         */
        'wxPay' => [
            'class'         => 'common\components\pay\Wx',
            'platId'        => 'wx',
            'appId'         => 'wxasdfafasfasdf',
            'merchantId'    => '12313132123',
            'bodyPrefix'    => '360tryst.com-',
            'key'           => 'sdfasdfasfda',
            'appSecret'     => 'asdfasdfasfdasfdasfdasfdasdf',
            'signKey'       => 'asdf23sdfasQ#EsdfsafdSDF123123sdf',
            'sslCertPath'   => \Yii::getAlias('@common').'/components/pay/cert/apiclient_cert.pem',
            'sslKeyPath'    => \Yii::getAlias('@common').'/components/pay/cert/apiclient_key.pem',
            'notifyUrl'     => ''
        ]
    ],
];
