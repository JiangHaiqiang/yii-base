<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午3:54
 */

return [
    'ORDER'      => '客户:{username} 在平台下单了，请注意查看',
    'ORDER_USER' => '感谢您的支持，下单成功我们会及时联系工作人员为您服务。服务电话：17531008546 投诉电话：0310-38828202。回T退订',
    'CHECK'      => '尊敬的用户您好，你在远鹏贸易有限公司申请的经销商加盟已经审核通过，感谢您的加入',
];