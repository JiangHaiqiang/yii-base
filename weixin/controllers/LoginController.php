<?php
/**
 * Created by PhpStorm.
 * User: Jiang Haiqiang
 * Date: 2018/11/24
 * Time: 下午8:17
 */

namespace weixin\controllers;


use common\helpers\CookieHelper;
use common\models\User;

/**
 * Class LoginController
 * @package weixin\controllers
 * User Jiang Haiqiang
 */
class LoginController extends Controller
{
    /**
     * @var bool
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public $enableCsrfValidation = false;

    /**
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function actionWxAuthCallback()
    {
        /**
         * @var User $userInfo
         */
        $userInfo = $this->_wxPublicLogin->authCallback();

        if($userInfo instanceof User) {
            if(isset($userInfo->id)) {
                $token = $this->_createToken($userInfo->id);

                CookieHelper::set(\Yii::$app->params['tokenKey'],$token);

                return $this->redirect(['/']);
            }
        }
        exit();
    }

    /**
     * @author Jiang Haiqiang
     * @email  jhq0113@163.com
     */
    public function actionWx()
    {
        $this->_wxPublicLogin->auth();
    }
}